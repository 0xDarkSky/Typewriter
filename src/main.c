#include <stdio.h>
#include <unistd.h>

void typewriter(char message[]) {
  int i = 0;

  while (message[i] != '\0') {
    printf("%c", message[i]);
    i++;
    fflush(stdout);
    usleep(50000); // modify this as you want
  }
}

int main(){
    typewriter("Hello World");
}